import Vue from 'vue';
import Router from 'vue-router';
import AppLayout from './views/AppLayout.vue';
import AuthLayout from './views/AuthLayout.vue';

import LoginPage from './components/auth/Login.vue';
import ModelsList from './components/app/ModelsList.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/app',
      name: 'app',
      component: AppLayout,
      redirect: 'models-list',
      children: [
        {
          path: '/models-list',
          name: 'modelsList',
          component: ModelsList,
        },
        {
          path: '/model/:id',
          name: 'model',
          component: () => import('./components/app/ModelPage.vue'),
        },
        {
          path: '/profiling',
          name: 'profiling',
          component: () => import('./components/app/ProfilingPage.vue'),
        },
        {
          path: '/summary',
          name: 'summary',
          component: () => import('./components/app/SummaryPage.vue'),
        },
        {
          path: '/dashboard',
          name: 'dashboard',
          component: () => import('./components/app/Dashboard.vue'),
        },
      ],
    },
    {
      path: '/',
      name: 'auth',
      component: AuthLayout,
      redirect: 'login',
      children: [
        {
          path: '/login',
          name: 'login',
          component: LoginPage,
        },
        {
          path: '/forgot-password',
          name: 'forgot-password',
          component: () => import('./components/auth/ForgotPassword.vue'),
        },
        {
          path: '/reset-password',
          name: 'resetPass',
          component: () => import('./components/auth/ResetPassword.vue'),
        },
        {
          path: '/register-password',
          name: 'registerPass',
          component: () => import('./components/auth/RegisterPass.vue'),
        },
      ],
    },
  ],
});
