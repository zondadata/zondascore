/* eslint-disable no-param-reassign */

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import state from './store/state';
import * as actions from './store/actions';
import * as mutations from './store/mutations';
import * as getters from './store/getters';

import toastr from './store/modules/toastr';
import model from './store/modules/model';

export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters,
  modules: {
    toastr,
    model,
  }
});
