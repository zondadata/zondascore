  export const spinner = (state) => {
    return state.spinner;
  };

  // export const // toastr = (state) => {
  //   return state.toastr;
  // },

  export const file = (state) => {
    return state.file.parse;
  };

  export const file_name = (state) => {
    return state.file.name;
  };

  export const file_size = (state) => {
    return state.file.size;
  };

  export const file_type = (state) => {
    return state.file.type;
  };

  export const edited_templates = (state) => {
    return state.edited.templates;
  };

  export const edited_fields = (state) => {
    return state.edited.fields;
  };

  export const edited_data = (state) => {
    return state.edited.data;
  };

  export const profiling_csv = (state) => {
    return state.profiling.csv;
  };

  export const profiling_json = (state) => {
    return state.profiling.json;
  };

  export const result = (state) => {
    return state.result;
  };

  export const template = (state) => {
    const temp = [];

    Object.entries(state.template).forEach(([key, value]) => {
      const index = parseInt(key, 10);
      temp.push({
        name: value,
        value: index,
      });
    });

    return temp;
  };
