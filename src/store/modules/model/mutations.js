export const ADD_MODEL = (state, model) => {
  state.model = model;
};

export const REMOVE_MODEL = (state) => {
  state.model = {
    uploaded: false,
    name: '',
    description: '',
  };
};
