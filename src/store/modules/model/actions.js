export const addModel = ({ commit }, model) => {
  commit('ADD_MODEL', model);
};

export const removeModel = ({ commit }) => {
  commit('REMOVE_MODEL');
};
