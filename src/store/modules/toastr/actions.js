export const showToastr = ({ commit }, toastr) => {
  commit('SHOW_TOASTR', toastr);
}

export const hideToastr = ({ commit }) => {
  commit('HIDE_TOASTR');
}
