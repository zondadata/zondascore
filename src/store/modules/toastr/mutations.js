export const SHOW_TOASTR = (state, toastr) => {
  state.toastr = { text: toastr.text, type: toastr.type, show: true };
}

export const HIDE_TOASTR = (state) => {
  state.toastr = {
    show: false,
  };
}
