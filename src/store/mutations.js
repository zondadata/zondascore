// SHOW_TOASTR(state, toastr) {
//   state.toastr = { text: toastr.text, type: toastr.type, show: true };
// },
// HIDE_TOASTR(state) {
//   state.toastr = {
//     show: false,
//   };
// },
export const SHOW_SPINNER = (state, spinner) => {
  state.spinner = { text: spinner, show: true };
};

export const HIDE_SPINNER = (state) => {
  state.spinner = {
    show: false,
    text: '',
  };
};

export const ADD_FILE = (state, file) => {
  state.file = { ...state.file, ...file };
};

export const ADD_TEMPLATE = (state, template) => {
  state.template = template;
};

export const REMOVE_EDITED_POSITION = (state) => {
  state.edited.templates = {};
};

export const SAVE_EDITED = (state, edited) => {
  state.edited = { ...state.edited, ...edited };
};

export const SAVE_EDITED_POSITION = (state, position) => {
  const key = Object.keys(position);

  if (state.edited.templates[key] !== undefined) {
    const newValue = position[key];

    if (newValue !== null) {
      const index = Object.values(state.edited.templates)
        .findIndex(item => item === newValue);

      if (index !== -1) {
        state.edited.templates[index] = state.edited.templates[key];
      }
    }

    state.edited.templates[key] = newValue;
  } else {
    state.edited.templates = { ...state.edited.templates, ...position };
  }
};

export const SAVE_PROFILING = (state, profiling) => {
  state.profiling = profiling;
};

export const SAVE_RESULT = (state, result) => {
  state.result = result;
};
