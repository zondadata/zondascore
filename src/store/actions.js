
// showToastr({ commit }, toastr) {
//   commit('SHOW_TOASTR', toastr);
// },
// hideToastr({ commit }) {
//   commit('HIDE_TOASTR');
// },
export const showSpinner = ({ commit }, spinner) => {
  commit('SHOW_SPINNER', spinner);
};
export const hideSpinner = ({ commit }) => {
  commit('HIDE_SPINNER');
};
export const addFile = ({ commit }, file) => {
  commit('ADD_FILE', file);
};

export const removeEditedPosition = ({ commit }) => {
  commit('REMOVE_EDITED_POSITION');
};

export const saveEdited = ({ commit }, edited) => {
  commit('SAVE_EDITED', edited);
};
export const saveEditedPosition = ({ commit }, position) => {
  commit('SAVE_EDITED_POSITION', position);
};
export const saveProfiling = ({ commit }, profiling) => {
  commit('SAVE_PROFILING', profiling);
};
export const saveResult = ({ commit }, result) => {
  commit('SAVE_RESULT', result);
};
export const addTemplate = ({ commit }, template) => {
  commit('ADD_TEMPLATE', template);
};
