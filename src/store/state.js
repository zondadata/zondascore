export default {
  spinner: {
    show: false,
    text: '',
  },
  // toastr: {
  //   text: '',
  //   type: '',
  //   show: false,
  // },
  file: {
    parse: {
      data: [],
      meta: {
        fields: [],
      },
    },
    name: '',
    type: '',
    size: 0,
  },
  edited: {
    data: [],
    fields: null,
    templates: [],
  },
  profiling: {
    csv: null,
    json: [],
  },
  result: [],
  template: {},
}
