import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import NProgress from 'nprogress' // Progress Индикатор выполнения
import 'nprogress/nprogress.css' // Progress Стиль выполнения
import firebase from 'firebase';

import VuePaginate from 'vue-paginate';
import moment from 'vue-moment';

import firebaseConfig from './configs/firebase';

Vue.config.productionTip = false;

Vue.use(VuePaginate);
Vue.use(moment);

const config = firebaseConfig.connection;

firebase.initializeApp(config);

NProgress.configure({ showSpinner: false });

router.beforeEach(
  (to, from, next) => {
    NProgress.start();
    store.dispatch('showSpinner', '');
    next();

  //   if (to.matched.some(record => record.meta.forVisitors)) {
  //     if (Vue.auth.isAuthenticated()) {
  //       next({
  //         name: URL.DEFAULT_APP_URL
  //       })
  //     } else next()
  //   } else if (to.matched.some(record => record.meta.forAuth)) {
  //     if (! Vue.auth.isAuthenticated()) {
  //       next({
  //         name: 'Login'
  //       })
  //       NProgress.done();
  //     } else next()
  //   } else next()

  }
);

router.afterEach(() => {
  store.dispatch('hideSpinner');
  NProgress.done();
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
