export default (config, callBack = null) => {
  function initViz() {
    let viz;

    const url = config.path,
      options = {
        hideTabs: false,
        hideToolbar: true,
        onFirstInteractive(e) {
          if (callBack && typeof callBack === 'function') {
            viz.addEventListener(tableau.TableauEventName.FILTER_CHANGE, callBack);
          }
        }
      };

    viz = new tableau.Viz(config.elem, url, options);
  }

  initViz();
}
