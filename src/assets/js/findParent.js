export default (node, selector) => {
  let condition =
    typeof selector === "string"
      ? item => item.matches(selector)
      : item => item === selector;

  let notFind = true;
  while (node.parentNode) {
    if (condition(node)) {
      notFind = false;
      break;
    }
    node = node.parentNode;
  }
  return notFind ? false : node;
}
