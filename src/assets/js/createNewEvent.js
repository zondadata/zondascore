export default (el, nameEvent, params) => {
  if (params && typeof params === 'object') {
    try {
      new CustomEvent("IE has CustomEvent, but doesn't support constructor");
    } catch (e) {

      window.CustomEvent = function (event, params) {
        let evt;
        params = params || {
          bubbles: false,
          cancelable: false,
          detail: undefined
        };
        evt = document.createEvent("CustomEvent");
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
      };

      CustomEvent.prototype = Object.create(window.Event.prototype);
    }
  }

  function createNewEvent(eventName) {
    let event;
    if (typeof (Event) === 'function') {
      if (params && typeof params === 'object') {
        event = new CustomEvent(eventName, {
          detail: params
        });
      } else {
        event = new Event(eventName);
      }
    } else {
      if (params && typeof params === 'object') {
        event = new CustomEvent(eventName, {
          detail: params
        });
      } else {
        event = document.createEvent('Event');
        event.initEvent(eventName, true, true);
      }
    }
    return event;
  }

  let event = createNewEvent(nameEvent);
  el.dispatchEvent(event);
}
