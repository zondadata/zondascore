export default {
  connection: {
  // Project on Mecha
  //   apiKey: "AIzaSyCcSMLk0KwZzldrooM3XhokU3cYyDNocKY",
  //   authDomain: "zonderscore.firebaseapp.com",
  //   databaseURL: "https://zonderscore.firebaseio.com",
  //   projectId: "zonderscore",
  //   storageBucket: "zonderscore.appspot.com",
  //   messagingSenderId: "902541094246"
  // Project on TesterCrisp
  //   apiKey: 'AIzaSyDGjd0R8245IKfA33dtdqAEVDAACCB0Yl0',
  //   authDomain: 'zonda-score-b7520.firebaseapp.com',
  //   databaseURL: 'https://zonda-score-b7520.firebaseio.com',
  //   projectId: 'zonda-score-b7520',
  //   storageBucket: 'zonda-score-b7520.appspot.com',
  //   messagingSenderId: '920331160899',

  // Project on ZondaData@gmail.com
    apiKey: "AIzaSyBYhUCzIfXajNYq9Zp5aEVI8gifFTkZ6ac",
    authDomain: "zondascore.firebaseapp.com",
    databaseURL: "https://zondascore.firebaseio.com",
    projectId: "zondascore",
    storageBucket: "zondascore.appspot.com",
    messagingSenderId: "550589817654"
  },

  path: {
    database: {
      csv: 'csv-files',
      models: 'models',
    },
    store: {
      csv: 'files_csv',
      results: 'files_results',
    },
  },
};
